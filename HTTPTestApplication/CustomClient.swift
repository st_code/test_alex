//
//  CustomClient.swift
//  HTTPTestApplication
//
//  Created by Alexander Akimov on 3/20/17.
//  Copyright © 2017 Alexander Akimov. All rights reserved.
//

import Foundation

class CustomClient: AAHttpClient {
    
    func testRequestWithParameters() {
        let url = URL(string: "http://localhost:1080/request/with/parameters?param1=value1&param2&value2")!
        let urlRequest = URLRequest(url: url)
        
        perform(urlRequest) { (data, response, error) in
            if let error = error {
                NSLog("Client: Response error: \(error.localizedDescription)")
            } else if let response = response as? HTTPURLResponse, !(200 ... 299).contains(response.statusCode) {
                NSLog("Client: Response HTTP error: \(response.statusCode.httpErrorCode)")
            } else {
                if let data = data,
                    let stringData = String(data: data, encoding:.utf8) as String! {
                    NSLog("Client: Got response: \(stringData)")
                }
            }
        }
    }
    
    func testRequestWithJsonResponse() {
        let url = URL(string: "http://localhost:1080/json")!
        let urlRequest = URLRequest(url: url)
        
        performJson(urlRequest) { (dictionary, response, error) in
            if let error = error {
                NSLog("Client: Response error: \(error.localizedDescription)")
            } else if let response = response as? HTTPURLResponse, !(200 ... 299).contains(response.statusCode) {
                NSLog("Client: Response HTTP error: \(response.statusCode.httpErrorCode)")
            } else {
                if let dictionary = dictionary {
                    NSLog("Client: Got JSON response:\n  title: \"\(dictionary["title"]!)\"\n  description: \"\(dictionary["description"]!)\"")
                }
            }
        }
    }
    
    func testLongRunningRequest() {
        let url = URL(string: "http://localhost:1080/longrunning")!
        let urlRequest = URLRequest(url: url)
        
        perform(urlRequest) { (data, response, error) in
            if let error = error {
                NSLog("Client: Response error: \(error.localizedDescription)")
            } else if let response = response as? HTTPURLResponse, !(200 ... 299).contains(response.statusCode) {
                NSLog("Client: Response HTTP error: \(response.statusCode.httpErrorCode)")
            } else {
                if let data = data,
                    let stringData = String(data: data, encoding:.utf8) as String! {
                    NSLog("Client: Got response: \"\(stringData)\"")
                }
            }
        }
    }
    
    func testCancellingLongRunningRequest() {
        let url = URL(string: "http://localhost:1080/longrunning2")!
        let urlRequest = URLRequest(url: url)
        
        let cancelation = perform(urlRequest) { (data, response, error) in
            if let error = error {
                NSLog("Client: Response error: \(error.localizedDescription)")
            } else if let response = response as? HTTPURLResponse, !(200 ... 299).contains(response.statusCode) {
                NSLog("Client: Response HTTP error: \(response.statusCode.httpErrorCode)")
            } else {
                if let data = data,
                    let stringData = String(data: data, encoding:.utf8) as String! {
                    NSLog("Client: Got response: \"\(stringData)\"")
                }
            }
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            cancelation.cancel()
        }
    }
    
    func testNotImplementedRequestMethod() {
        let url = URL(string: "http://localhost:1080/json")!
        var urlRequest = URLRequest(url: url)
        
        // server implements only GET method for this Url, try PUT to get 405 error
        urlRequest.httpMethod = "PUT"
        
        perform(urlRequest) { (data, response, error) in
            if let error = error {
                NSLog("Client: Response error: \(error.localizedDescription)")
            } else if let response = response as? HTTPURLResponse, !(200 ... 299).contains(response.statusCode) {
                NSLog("Client: Response HTTP error: \(response.statusCode.httpErrorCode)")
            } else {
                if let data = data,
                    let stringData = String(data: data, encoding:.utf8) as String! {
                    NSLog("Client: Got response: \"\(stringData)\"")
                }
            }
        }
    }
    
    func test404error() {
        let url = URL(string: "http://localhost:1080/not_existing_page")!
        let urlRequest = URLRequest(url: url)
        
        perform(urlRequest) { (data, response, error) in
            if let error = error {
                NSLog("Client: Response error: \(error.localizedDescription)")
            } else if let response = response as? HTTPURLResponse, !(200 ... 299).contains(response.statusCode) {
                NSLog("Client: Response HTTP error: \(response.statusCode.httpErrorCode)")
            } else {
                if let data = data,
                    let stringData = String(data: data, encoding:.utf8) as String! {
                    NSLog("Client: Got response: \"\(stringData)\"")
                }
            }
        }
    }
}

private extension Int {
    var httpErrorCode: String {
        return String(self) + " - " + HTTPURLResponse.localizedString(forStatusCode: self)
    }
}
