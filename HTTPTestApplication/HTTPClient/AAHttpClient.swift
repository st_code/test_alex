//
//  AAHttpClient.swift
//  HTTPTestApplication
//
//  Created by Alexander Akimov on 3/18/17.
//  Copyright © 2017 Alexander Akimov. All rights reserved.
//

import Foundation

typealias JSONResponse = [String : Any]

class AAHttpClient: NSObject {
    
    private var session: URLSession
    
    init(maxConnectionsPerHost: Int = 4, timeout: TimeInterval = 60) {
        let config = URLSessionConfiguration.default
        config.httpMaximumConnectionsPerHost = maxConnectionsPerHost
        config.timeoutIntervalForResource = timeout
        session = URLSession(configuration: config)
        super.init()
    }
    
    @discardableResult
    public func perform(_ request: URLRequest, with completion: @escaping (Data?, URLResponse?, Error?) -> Void) -> Cancelable {
        // No need to create OperationQueue to serialize requests, because URLSession already has its own OperationQueue
        let dataTask = session.dataTask(with: request) { (data, response, error) in
            completion(data, response, error)
        }
        dataTask.resume()
        return AACancelationObject(dataTask)
    }
    
    //
    // Request that waits JSON in server response
    //
    @discardableResult
    public func performJson(_ request: URLRequest, with completion: @escaping (JSONResponse?, URLResponse?, Error?) -> Void) -> Cancelable {
        return perform(request) { (data, response, error) in
            if let data = data {
                do {
                    if let jsonData = try JSONSerialization.jsonObject(with: data, options: []) as? JSONResponse {
                        completion(jsonData, response, error)
                    } else {
                        completion(nil, response, error)
                    }
                }
                catch {
                    completion(nil, response, error)
                }
            } else {
                completion(nil, response, error)
            }
        }
    }
}
