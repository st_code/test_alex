//
//  AACancelationObject.swift
//  HTTPTestApplication
//
//  Created by Alexander Akimov on 3/20/17.
//  Copyright © 2017 Alexander Akimov. All rights reserved.
//

import Foundation

protocol Cancelable {
    func cancel()
}

class AACancelationObject: NSObject, Cancelable {
    private var dataTask: URLSessionDataTask
    
    init(_ dt: URLSessionDataTask) {
        dataTask = dt
        super.init()
    }
    
    func cancel() {
        if dataTask.state == .running || dataTask.state == .suspended {
            NSLog("Client: cancelling request")
            dataTask.cancel()
        }
    }
}
