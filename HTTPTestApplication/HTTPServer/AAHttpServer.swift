//
//  AAHttpServer.swift
//  HTTPTestApplication
//
//  Created by Alexander Akimov on 3/18/17.
//  Copyright © 2017 Alexander Akimov. All rights reserved.
//

import Foundation
import CocoaAsyncSocket
import CFNetwork

//
// Http server class.
// Implemented using GCDAsyncSocket (CocoaAsyncSocket framework) due to the lack of time to make 
// on raw sockets / NSInputStream + NSOutputStream
// CGDAsyncSockets handles streams itself: queueing, buffering, searching termination sequences;
// provides automatic socket acceptance; runs on its own dispatch queue
//

//
// Usage:
// - start(on:) - starts listening port
// - stop() - stops listening port
// - addHandler(for:path:handler:) - adds handler for your custom request
//
class AAHttpServer: NSObject {
    let defaultTimeout: TimeInterval = 30
    private lazy var socketQueue: DispatchQueue = {
        DispatchQueue(label: "TCPQueue")
    }()
    
    fileprivate lazy var listeningSocket: GCDAsyncSocket = {
        GCDAsyncSocket(delegate: self, delegateQueue: self.socketQueue)
    }()
    
    fileprivate var connectedSockets: [GCDAsyncSocket] = []
    private var isRunning = false
    
    private lazy var handlers: Dictionary<String, Dictionary<HttpMethod, (AAHttpServerRequest, @escaping (AAHttpServerResponse) -> Void) -> Void>> = {
        [:]
    }()
    
    fileprivate lazy var socketsToRequests: Dictionary<GCDAsyncSocket, AAHttpServerRequest> = {
        [:]
    }()
    
    required override init() {
        super.init()
    }
    
    // MARK: - Public methods
    
    public func start(on port: UInt16) {
        guard isRunning == false else {
            NSLog("Server: Already started")
            return
        }
        do {
            try listeningSocket.accept(onPort: port)
            isRunning = true
            NSLog("Server: started listening port: \(port)")
        }
        catch {
            NSLog("Server: Error start listening port: \(port)")
        }
    }
    
    public func stop() {
        listeningSocket.disconnect()
        _ = connectedSockets.map { $0.disconnect() }
        isRunning = false
    }
    
    //
    // Adds handler for your custom URL path and HTTP method
    // Handler can be synchronous/asynchronous and should return AAHttpServerRequest object in its completion block
    //
    public func addHandler(for method: HttpMethod, path: String, handler: @escaping (AAHttpServerRequest, @escaping (AAHttpServerResponse) -> Void) -> Void) {
        // check if any method handler exist for the given path
        var methodToHandlerDictionary = handlers[path] ?? [:]
        methodToHandlerDictionary[method] = handler
        handlers[path] = methodToHandlerDictionary
    }
    
    // MARK: - Private methods

    func processIncoming(data: Data, from socket: GCDAsyncSocket) {
        let incomingMessage = CFHTTPMessageCreateEmpty(kCFAllocatorDefault, true).takeRetainedValue()
        _ = data.withUnsafeBytes {
            CFHTTPMessageAppendBytes(incomingMessage, $0, data.count)
        }
        
        if CFHTTPMessageIsHeaderComplete(incomingMessage),
            let request = createRequest(from: incomingMessage) {
            socketsToRequests[socket] = request
            
            handle(request) { [weak self] response in
                guard let `self` = self else { return }
                request.isCompleted = true
                let responseMessage = self.createResponseMessage(from: response)
                self.save(responseMessage, to: socket)
            }
        } else {
            // can't create request for unknown reason
            let response = AAHttpServerResponse(errorCode: 500)
            let responseMessage = createResponseMessage(from: response)
            save(responseMessage, to: socket)
        }
    }
    
    func save(_ responseMessage: CFHTTPMessage, to socket: GCDAsyncSocket) {
        if let responseData = CFHTTPMessageCopySerializedMessage(responseMessage)?.takeRetainedValue() as? NSData as? Data {
            socket.write(responseData, withTimeout: defaultTimeout, tag: 0)
        }
    }
    
    func createRequest(from message: CFHTTPMessage) -> AAHttpServerRequest? {
        guard
            let methodString = CFHTTPMessageCopyRequestMethod(message)?.takeRetainedValue() as? String,
            let httpMethod = HttpMethod(rawValue: methodString),
            let url = CFHTTPMessageCopyRequestURL(message)?.takeRetainedValue() as? URL,
            let headers = CFHTTPMessageCopyAllHeaderFields(message)?.takeRetainedValue() as? [String : Any] else {
                return nil
        }
        
        let body = CFHTTPMessageCopyBody(message)?.takeRetainedValue() as? NSData as? Data
        
        return AAHttpServerRequest(method: httpMethod, path: url.absoluteString, headers: headers, body: body)
    }

    func createResponseMessage(from response: AAHttpServerResponse) -> CFHTTPMessage {
        let responseMessage = CFHTTPMessageCreateResponse(kCFAllocatorDefault, CFIndex(response.statusCode), nil, kCFHTTPVersion1_1).takeRetainedValue()
        
        for (key, value) in response.headers {
            if let stringValue = value as? String {
                CFHTTPMessageSetHeaderFieldValue(responseMessage, key as CFString, stringValue as CFString)
            }
        }
        
        if let responseBody = response.body {
            CFHTTPMessageSetHeaderFieldValue(responseMessage, "Content-Length" as CFString, "\(responseBody.count)" as CFString?)
            CFHTTPMessageSetBody(responseMessage, responseBody as CFData)
        } else {
            CFHTTPMessageSetHeaderFieldValue(responseMessage, "Content-Length" as CFString, "0" as CFString)
        }
        
        return responseMessage
    }
    
    func handle(_ request: AAHttpServerRequest, completion: @escaping (AAHttpServerResponse) -> Void) {
        NSLog("Server: request received: \(request.method.rawValue) \(request.path)")

        let fullPath = request.path
        
        guard let path = URL(string: fullPath)?.path,
            let methodToHandlerDictionary = handlers[path] else {
            completion(AAHttpServerResponse(errorCode: 404))
            return
        }
        
        guard let handler = methodToHandlerDictionary[request.method] else {
            completion(AAHttpServerResponse(errorCode: 405))
            return
        }
        
        handler(request) { response in
            completion(response)
        }
    }
}

extension AAHttpServer: GCDAsyncSocketDelegate {
    func socket(_ sock: GCDAsyncSocket, didAcceptNewSocket newSocket: GCDAsyncSocket) {
        connectedSockets.append(newSocket)
        if let host = newSocket.connectedHost {
            NSLog("Server: Accepted new connection from \(host):\(newSocket.connectedPort)")
        }
        newSocket.readData(withTimeout: defaultTimeout, tag: 0)
    }

    func socketDidDisconnect(_ sock: GCDAsyncSocket, withError err: Error?) {
        if sock != listeningSocket {
            NSLog("Server: Client disconnected")
            if let index = connectedSockets.index(of: sock) {
                connectedSockets.remove(at: index)
            }
            
            if let request = socketsToRequests[sock] {
                if request.isCompleted == false {
                    request.isCancelled = true
                    NSLog("Server: Client closed connection! Stopping processing the request!")
                }
                socketsToRequests.removeValue(forKey: sock)
            }
        }
    }
    
    func socket(_ sock: GCDAsyncSocket, didRead data: Data, withTag tag: Int) {
        processIncoming(data: data, from: sock)
    }
}
