//
//  AAHttpServerResponse.swift
//  HTTPTestApplication
//
//  Created by Alexander Akimov on 3/19/17.
//  Copyright © 2017 Alexander Akimov. All rights reserved.
//

import Foundation

class AAHttpServerResponse: NSObject {
    var headers: [String : Any]
    var body: Data?
    var statusCode: UInt16
    
    init(errorCode: UInt16) {
        headers = ["Connection" : "close"]
        statusCode = errorCode
        super.init()
    }
    
    init(text: String, headers: [String : Any]) {
        self.headers = headers
        self.headers["Content-Type"] = "text"
        self.headers["Connection"] = "close"
        body = text.data(using: .utf8)
        statusCode = (body?.count ?? 0) != 0 ? 200 : 204
        super.init()
    }
    
    init(dictionary: [String : Any], headers: [String : Any]) {
        do {
            body = try JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted)
            self.headers = headers
            self.headers["Content-Type"] = "application/json"
            self.headers["Connection"] = "close"
            statusCode = (body?.count ?? 0) != 0 ? 200 : 204
        } catch {
            self.headers = ["Connection" : "close"]
            statusCode = 500
        }
        super.init()
    }
    
    // Initializers to return files, etc. could be added
}
