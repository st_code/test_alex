//
//  AAHttpServerRequest.swift
//  HTTPTestApplication
//
//  Created by Alexander Akimov on 3/19/17.
//  Copyright © 2017 Alexander Akimov. All rights reserved.
//

import Foundation

enum HttpMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
}

class AAHttpServerRequest: NSObject {
    var method: HttpMethod
    var path: String
    var headers: [String : Any]
    var body: Data?
    
    // Server sets isCancelled to true when client breaks the connection while the request handler
    // is being executed on server
    var isCancelled = false
    
    // Server sets isCompleted to true when the request handler finishes its execution
    var isCompleted = false
    
    init(method: HttpMethod, path: String, headers: [String : Any], body: Data?) {
        self.method = method
        self.path = path
        self.headers = headers
        self.body = body
        super.init()
    }
    
    // extracts the parameter with given name from URL path 
    func getParameter(_ name: String) -> String? {
        let queryItems = URLComponents(string: path)?.queryItems
        let parameter = queryItems?.filter({$0.name == name}).first
        return parameter?.value
    }
}
