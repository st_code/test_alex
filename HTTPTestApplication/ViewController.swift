//
//  ViewController.swift
//  HTTPTestApplication
//
//  Created by Alexander Akimov on 3/18/17.
//  Copyright © 2017 Alexander Akimov. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    private lazy var server: AAHttpServer = {
        CustomServer()
    }()
    
    private lazy var client: CustomClient = {
        CustomClient(maxConnectionsPerHost: 4)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        server.start(on: 1080)
        
        // uncomment to see how request queueing is working
//        for _ in 0 ..< 20 {
//            client.testLongRunningRequest()
//        }
        
        client.testLongRunningRequest()
        client.testRequestWithJsonResponse()
        client.testCancellingLongRunningRequest()
        client.testNotImplementedRequestMethod()
        client.testRequestWithParameters()
        client.test404error()
    }
    
    deinit {
        server.stop()
    }
}

