//
//  CustomServer.swift
//  HTTPTestApplication
//
//  Created by Alexander Akimov on 3/20/17.
//  Copyright © 2017 Alexander Akimov. All rights reserved.
//

import Foundation

class CustomServer: AAHttpServer {
    required init() {
        super.init()
        
        addHandler(for: .get, path: "/") { (request, completion) in
            let response = AAHttpServerResponse(text: "Hello from Server!", headers: request.headers)
            completion(response)
        }

        addHandler(for: .get, path: "/json") { (request, completion) in
            let response = AAHttpServerResponse(dictionary: ["title" : "Hello from Server!",
                                                     "description" : "Demo of returning JSON data"],
                                        headers: request.headers)
            completion(response)
        }

        addHandler(for: .get, path: "/request/with/parameters") { (request, completion) in
            var text = "\"param1\" not found in the request"
            
            if let param1 = request.getParameter("param1") {
                text = "Extracted \"param1\" value: \"\(param1)\""
            }
            
            let response = AAHttpServerResponse(text: text, headers: request.headers)
            completion(response)

        }
        
        addHandler(for: .get, path: "/longrunning") { (request, completion) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                let response = AAHttpServerResponse(text: "Long running request 1 executed", headers: request.headers)
                completion(response)
            }
        }
        
        addHandler(for: .get, path: "/longrunning2") { [weak self] (request, completion) in
            // this aproch is used for test purpose
            // in other situation it may be more convinient to create observer (KVO) of request.isCancelled
            // if I have more time to implement server on raw sockets / NSInputStream + NSOutputStream, then we can check the cancellation state before writing every portion of data to output stream
            self?.waitFor(timeout: 10, orCancellationOf: request) {
                let response = AAHttpServerResponse(text: "Long running request 2 executed", headers: request.headers)
                completion(response)
            }
        }
    }
    
    func waitFor(timeout seconds: Int, orCancellationOf request: AAHttpServerRequest, completion: @escaping () -> Void) {
        NSLog("Server: \(seconds) seconds left to complete GET /longrunning2 request")
        let secondsLeft = seconds - 1
        if secondsLeft >= 0 {
            if request.isCancelled == false {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
                    self?.waitFor(timeout: secondsLeft, orCancellationOf: request, completion: completion)
                }
            } else {
                NSLog("Server: Stop processing GET /longrunning2 request as is it aborted from client side")
            }
        } else {
            completion()
        }
    }
}
