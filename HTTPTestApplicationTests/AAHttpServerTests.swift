//
//  AAHttpServerTests.swift
//  HTTPTestApplication
//
//  Created by Alexander Akimov on 3/21/17.
//  Copyright © 2017 Alexander Akimov. All rights reserved.
//

import XCTest
import CocoaAsyncSocket
@testable import HTTPTestApplication

class AAHttpServerTests: XCTestCase {
    var server: AAHttpServer?
    
    override func setUp() {
        super.setUp()
        let server = AAHttpServer()
        self.server = server
        server.addHandler(for: .get, path: "/defined_request") { (request, completion) in
            let response = AAHttpServerResponse(text: "I'm OK", headers: request.headers)
            completion(response)
        }
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testHandlingUndefinedRequest() {
        guard let server = self.server else {
            XCTFail("Server is not created")
            return
        }
        
        let request = AAHttpServerRequest(method: .get, path: "/undefined_path", headers: [ : ], body: nil)
        
        let processExpectation = expectation(description: "GET /undefined_path")
        
        server.handle(request) { response in
            XCTAssertEqual(response.statusCode, 404, "Status code should be 404!")
            processExpectation.fulfill()
        }
        
        waitForExpectations(timeout: 1) { error in
            if let error = error {
                XCTFail("Request is not completed, error: \(error)")
            }
        }
    }
    
    func testHandlingDefinedRequest() {
        guard let server = self.server else {
            XCTFail("Server is not created")
            return
        }
        
        let request = AAHttpServerRequest(method: .get, path: "/defined_request", headers: [ : ], body: nil)
        
        let processExpectation = expectation(description: "GET /defined_request")
        
        server.handle(request) { response in
            XCTAssertEqual(response.statusCode, 200, "Status code should be 200!")
            XCTAssertNotNil(response.body, "Response body should not be nil!")
            let text = String(data: response.body!, encoding: .utf8)
            XCTAssertEqual(text, "I'm OK")

            processExpectation.fulfill()
        }
        
        waitForExpectations(timeout: 1) { error in
            if let error = error {
                XCTFail("Request is not completed, error: \(error)")
            }
        }
    }
    
    func testHandlingRawBytes() {
        guard let server = self.server else {
            XCTFail("Server is not created")
            return
        }
        
        class MockSocket: GCDAsyncSocket {
            var didWriteData: (Data) -> Void
            
            init(with callback: @escaping (Data) -> Void) {
                self.didWriteData = callback
                super.init(delegate: nil, delegateQueue: nil, socketQueue: nil)
            }
            
            override func write(_ data: Data, withTimeout timeout: TimeInterval, tag: Int) {
                didWriteData(data)
            }
        }
        
        let requestText = "GET /defined_request HTTP/1.1" + "\r\n" +
            "Host: localhost:1080" + "\r\n" +
            "Accept: */*" + "\r\n" + "\r\n" +
            "Accept-Language: en-us" + "\r\n" +
            "Connection: keep-alive" + "\r\n" +
            "Accept-Encoding: gzip, deflate" + "\r\n" +
            "User-Agent: HTTPTestApplication/1 CFNetwork/808.2.16 Darwin/15.6.0"
 
        let requestData = requestText.data(using: .utf8)!
        
        let processExpectation = expectation(description: "RAW: GET /defined_request")

        let socket = MockSocket() { data in
            let text = String(data: data, encoding: .utf8)
            XCTAssertNotNil(text, "Response data should be a text!")
            XCTAssertTrue(text!.contains("200 OK"), "Response code should be 200!")
            XCTAssertTrue(text!.contains("I'm OK"), "Response code should contain text: \"I'm OK\"!")
            processExpectation.fulfill()
        }
        
        server.processIncoming(data: requestData, from: socket)
        
        waitForExpectations(timeout: 1) { error in
            if let error = error {
                XCTFail("Request is not completed, error: \(error)")
            }
        }
    }
}
